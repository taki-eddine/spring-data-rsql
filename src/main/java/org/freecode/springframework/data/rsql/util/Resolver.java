package org.freecode.springframework.data.rsql.util;

public interface Resolver<T> {

	T resolve();

}
