package org.freecode.springframework.data.rsql.util;

import lombok.Getter;
import lombok.Value;

import javax.persistence.criteria.Path;
import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@Value(staticConstructor = "of")
@Getter(PRIVATE)
public class ArgsResolver<T> implements Resolver<Object> {

	List<String> values;
	Path<T> path;

	@Override
	public Object resolve() {
		return values.get(0);
	}

}
