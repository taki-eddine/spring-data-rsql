package org.freecode.springframework.data.rsql.service.impl;

import cz.jirutka.rsql.parser.RSQLParser;
import lombok.extern.slf4j.Slf4j;
import org.freecode.springframework.data.rsql.constant.SupportedRsqlOperators;
import org.freecode.springframework.data.rsql.service.RsqlService;
import org.freecode.springframework.data.rsql.util.SpecificationRSQLVisitor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@Slf4j
public class RsqlServiceImpl<T> implements RsqlService<T> {

	RSQLParser rsqlParser = new RSQLParser(SupportedRsqlOperators.ALL_OPERATORS);
	SpecificationRSQLVisitor<T> specificationRSQLVisitor = new SpecificationRSQLVisitor<>();

	@Override
	@Nullable
	public Specification<T> parse(@Nullable String rsql) {
		if (StringUtils.isEmpty(rsql)) {
			return null;
		}

		return rsqlParser.parse(rsql).accept(specificationRSQLVisitor);
	}

}
