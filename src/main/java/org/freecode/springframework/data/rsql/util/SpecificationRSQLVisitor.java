package org.freecode.springframework.data.rsql.util;

import cz.jirutka.rsql.parser.ast.*;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Path;
import java.util.function.BinaryOperator;

@Slf4j
@NoArgsConstructor
public class SpecificationRSQLVisitor<T> extends NoArgRSQLVisitorAdapter<Specification<T>> {

	@Override
	public Specification<T> visit(AndNode node) {
		log.debug("Visiting a 'and' node: node='{}'", node);
		return visit(node, Specification::and);
	}

	@Override
	public Specification<T> visit(OrNode node) {
		log.debug("Visiting a 'or' node: node='{}'", node);
		return visit(node, Specification::or);
	}

	@Override
	public Specification<T> visit(ComparisonNode node) {
		log.debug("Visiting a 'comparison' node: node='{}'", node);

		return (root, query, builder) -> {
			Path<?> path = PathResolver.of(node.getSelector(), root).resolve();
			Object args = ArgsResolver.of(node.getArguments(), path).resolve();
			return CriteriaResolver.of(node.getOperator(), path, args, builder).resolve();
		};
	}

	private Specification<T> visit(LogicalNode node, BinaryOperator<Specification<T>> accumulator) {
		return node.getChildren()
			.stream()
			.map(n -> n.accept(this))
			.reduce(accumulator)
			.orElseThrow(IllegalStateException::new);
	}

}
