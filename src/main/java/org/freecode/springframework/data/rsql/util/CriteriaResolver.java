package org.freecode.springframework.data.rsql.util;

import cz.jirutka.rsql.parser.ast.ComparisonOperator;
import lombok.Getter;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

import static lombok.AccessLevel.PRIVATE;
import static org.freecode.springframework.data.rsql.constant.SupportedRsqlOperators.*;

@Slf4j
@Value(staticConstructor = "of")
@Getter(PRIVATE)
public class CriteriaResolver<T, U> implements Resolver<Predicate> {

	ComparisonOperator operator;
	Path<T> path;
	U args;
	CriteriaBuilder builder;

	@Override
	public Predicate resolve() {
		log.debug("Resolve criteria of: operator='{}'", operator);

		if (operator.equals(EQUAL)) {
			return builder.equal(path, args);
		} else if (operator.equals(NOT_EQUAL)) {
			return builder.notEqual(path, args);
		} else if (operator.equals(LESS_THAN)) {
			return builder.lessThan((Path) path, (Comparable) args);
		} else if (operator.equals(LESS_THAN_OR_EQUAL)) {
			return builder.lessThanOrEqualTo((Path) path, (Comparable) args);
		} else if (operator.equals(GREATER_THAN)) {
			return builder.greaterThan((Path) path, (Comparable) args);
		} else if (operator.equals(GREATER_THAN_OR_EQUAL)) {
			return builder.greaterThanOrEqualTo((Path) path, (Comparable) args);
		} else if (operator.equals(LIKE)) {
			return builder.like(builder.lower((Path) path), "%" + ((String) args).toLowerCase() + "%");
		}

		log.error("Unknown operator='{}'", operator);
		throw new IllegalArgumentException("Unknown operator='" + operator + "'");
	}

}
