package org.freecode.springframework.data.rsql.service;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;

public interface RsqlService<T> {

    @Nullable
    Specification<T> parse(@Nullable String rsql);

}
