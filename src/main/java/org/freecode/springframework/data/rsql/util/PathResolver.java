package org.freecode.springframework.data.rsql.util;

import lombok.Getter;
import lombok.Value;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import static lombok.AccessLevel.PRIVATE;

@Value(staticConstructor = "of")
@Getter(PRIVATE)
public class PathResolver<T> implements Resolver<Path> {

	String selector;
	Root<T> root;

	@Override
	public Path<?> resolve() {
		String[] pathTokens = selector.split("\\.");
		Path path = root.get(pathTokens[0]);

		for (int i = 1; i < pathTokens.length; i++) {
			path = path.get(pathTokens[i]);
		}

		return path;
	}

}
