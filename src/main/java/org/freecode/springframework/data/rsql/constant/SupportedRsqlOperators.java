package org.freecode.springframework.data.rsql.constant;

import cz.jirutka.rsql.parser.ast.ComparisonOperator;
import lombok.experimental.UtilityClass;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;

/**
 * Supported RSQL operators.
 */
@UtilityClass
public class SupportedRsqlOperators {

	/**
	 * Equal operator symbols representation.
	 */
	public static final ComparisonOperator EQUAL = new ComparisonOperator("==");

	/**
	 * Not Equal operator symbols representation.
	 */
	public static final ComparisonOperator NOT_EQUAL = new ComparisonOperator("!=");

	/**
	 * Less Than operator symbols representation.
	 */
	public static final ComparisonOperator LESS_THAN = new ComparisonOperator("<");

	/**
	 * Less Than Or Equal operator symbols representation.
	 */
	public static final ComparisonOperator LESS_THAN_OR_EQUAL = new ComparisonOperator("<=");

	/**
	 * Greater Than operator symbols representation.
	 */
	public static final ComparisonOperator GREATER_THAN = new ComparisonOperator(">");

	/**
	 * Greater Than Or Equal operator symbols representation.
	 */
	public static final ComparisonOperator GREATER_THAN_OR_EQUAL = new ComparisonOperator(">=");

	/**
	 * Like operator symbols representation.
	 */
	public static final ComparisonOperator LIKE = new ComparisonOperator("=like=");

	/**
	 * All Supported Operators.
	 */
	public static final Set<ComparisonOperator> ALL_OPERATORS = Collections.unmodifiableSet(
		new HashSet<>(
			asList(
				EQUAL,
				NOT_EQUAL,
				LESS_THAN,
				LESS_THAN_OR_EQUAL,
				GREATER_THAN,
				GREATER_THAN_OR_EQUAL,
				LIKE
			)
		)
	);

}
