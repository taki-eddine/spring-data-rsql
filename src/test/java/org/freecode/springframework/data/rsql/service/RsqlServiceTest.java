package org.freecode.springframework.data.rsql.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.freecode.springframework.data.rsql.service.domain.User;
import org.freecode.springframework.data.rsql.service.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest(showSql = false)
@EnableAutoConfiguration
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class RsqlServiceTest {

	final UserRepository userRepository;
	final RsqlService<User> rsqlService;

	@BeforeEach
	void setUp() {
		userRepository.saveAll(asList(
			new User("first_name_1", "last_name_1"),
			new User("first_name_2", "last_name_2"),
			new User("first_name_3", "last_name_3")
		));
	}

	@Test
	void parse_RsqlQueryOfNull_ShouldGiveNull() {
		// arrange
		String rsql = null;

		// act
		Specification<User> specification = rsqlService.parse(rsql);

		// assert
		assertNull(specification);
	}

	@Test
	void parse_RsqlQueryUsesLogicalOperatorAndOrWithComparisonOperatorEqual_ShouldFindTwoEntities() {
		// arrange
		String rsql = "firstName==first_name_1;lastName==last_name_1,firstName==first_name_2";

		// act
		Specification<User> specification = rsqlService.parse(rsql);

		Specification<User> spec1 = (root, query, builder) -> builder.equal(root.get("firstName"), "first_name_1");
		Specification<User> spec2 = (root, query, builder) -> builder.equal(root.get("lastName"), "last_name_1");
		Specification<User> spec3 = (root, query, builder) -> builder.equal(root.get("firstName"), "first_name_2");

		List<User> usersRsql = userRepository.findAll(specification);
		List<User> users = userRepository.findAll(spec1.and(spec2).or(spec3));

		// assert
		assertEquals(users, usersRsql);
	}

}
