package org.freecode.springframework.data.rsql.service.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = "users")
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User {

	@Id
	@GeneratedValue(strategy = SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	Long id;

	@Column(name = "first_name", nullable = false)
	@EqualsAndHashCode.Include
	String firstName;

	@Column(name = "last_name", nullable = false)
	@EqualsAndHashCode.Include
	String lastName;

	public User(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

}
