package org.freecode.springframework.data.rsql;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootConfiguration
@ComponentScan
public class TestSpringDataRsqlApplicationConfiguration {

}
